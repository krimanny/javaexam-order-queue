package com.java.exam.consumer;

import com.java.exam.config.AppConfig;
import com.java.exam.dto.OrderResponseDto;
import com.java.exam.entity.Order;
import com.java.exam.entity.OrderItem;
import com.java.exam.enums.StatusEnum;
import com.java.exam.service.OrderItemService;
import com.java.exam.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component
public class OrderConsumer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderItemService orderItemService;

    @RabbitListener(queues = AppConfig.ORDER_QUEUE, concurrency = "5")
    public void consumeOrder(Order order) throws IOException {
        log.info("processing orderRequest={}", order);
        OrderResponseDto orderResponseDto = new OrderResponseDto();
        List<OrderItem> orderItems = orderItemService.findAllByOrderId(order.getId());
        if (!orderItems.isEmpty()) {
            for (OrderItem item : orderItems) {
                int reserved = orderItemService.countAllStocksReserved(item.getProductId());
                log.info("Remaining stocks :" + reserved);
                if (reserved < item.getQuantity()) {
                    order.setStatus(StatusEnum.INSUFFICIENT_STOCKS.value);
                    orderResponseDto.setError_message("Insufficient stocks for product=" + item.getProductId());
                    orderService.save(order);
                    break;
                }
            }
            if (null != order.getStatus() && !order.getStatus().equals(StatusEnum.INSUFFICIENT_STOCKS.value)) {
                order.setStatus(StatusEnum.RESERVED.value);
                order = orderService.save(order);
            }
        }else{
            order.setStatus(StatusEnum.REJECTED.value);
            orderResponseDto.setError_message("Product are not found");
        }
        log.info("Complete Order request={} " + order.getStatus(), order);
        orderResponseDto.setOrder_id(String.valueOf(order.getId()));
        orderResponseDto.setOrder_status(order.getStatus());

        /**
         * Sending to order response queue
        */
        rabbitTemplate.convertAndSend(AppConfig.ORDER_EXCHANGE, AppConfig.RESPONSE_ROUTE_KEY, orderResponseDto);
        log.info("Send to order response queue response={}", orderResponseDto);
    }

    @RabbitListener(queues = AppConfig.ORDER_RESPONSE_QUEUE, concurrency = "5")
    public void consumeOrderResponse(OrderResponseDto orderResponseDto){
        log.info("Received order response={}", orderResponseDto);
    }

}
