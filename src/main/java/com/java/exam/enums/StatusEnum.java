package com.java.exam.enums;

public enum StatusEnum {
    ORDER_RECEIVED("ORDER_RECEIVED"),
    REJECTED("REJECTED"),
    RESERVED("RESERVED"),
    INSUFFICIENT_STOCKS("INSUFFICIENT_STOCKS");

    public String value;

    private StatusEnum(String value){
        this.value = value;
    }
}
