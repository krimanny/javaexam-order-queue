package com.java.exam.controller;

import com.java.exam.config.AppConfig;
import com.java.exam.dto.OrderItemDto;
import com.java.exam.dto.OrderRequestDto;
import com.java.exam.entity.Order;
import com.java.exam.entity.OrderItem;
import com.java.exam.entity.Product;
import com.java.exam.enums.StatusEnum;
import com.java.exam.service.OrderItemService;
import com.java.exam.service.OrderService;
import com.java.exam.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class OrderController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderItemService orderItemService;

    @PostMapping("/createOrder")
    public String postOrder(@RequestBody OrderRequestDto orderRequestDto){
        Order order = new Order();
        List<OrderItem> orderItem= new ArrayList<>();
        List<String> products = new ArrayList();
        for(OrderItemDto item : orderRequestDto.getItems()){
            Product product = productService.getProduct(item.getProduct_id());
            if(product != null){
                OrderItem newOrder = new OrderItem(item.getProduct_id(), item.getQuantity());
                orderItem.add(newOrder);
                products.add(product.getName());
            }else {
                log.info("product={} is not found", item);
            }
        }
        order.setProducts(String.join(",", products));
        order.setClientName(orderRequestDto.getClient_name());
        order.setStatus(StatusEnum.ORDER_RECEIVED.value);
        order = orderService.save(order);

        for(OrderItem orderItem1: orderItem){
            orderItem1.setOrder(order);
            orderItemService.save(orderItem1);
        }
        rabbitTemplate.convertAndSend(AppConfig.ORDER_EXCHANGE, AppConfig.ROUTE_KEY, order);
        log.info("Received Order Request={}", orderRequestDto);
        return "order received";
    }
}
