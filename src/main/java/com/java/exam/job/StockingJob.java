package com.java.exam.job;

import com.java.exam.dto.StockDto;
import com.java.exam.entity.Product;
import com.java.exam.service.ProductService;
import com.java.exam.utils.DateTimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class StockingJob {

    @Autowired
    ProductService productService;

    @Value("${input.folder}")
    private String inputFolderPath;

    @Value("${processed.folder}")
    private String processFolderPath;

    @Scheduled(fixedRate = 10000)
    public void loadXml() throws Exception {
        File dir = new File(inputFolderPath);
        if(dir.exists()) {
            File[] directoryListing = dir.listFiles();
            if (directoryListing.length > 0) {
                for (File fileToRead : directoryListing) {
                Optional<String> filename = getExtension(fileToRead.getName());
                if(!filename.isEmpty() && filename.get().equals("xml"))
                    log.info("reading xml={}", fileToRead.getName());
                    getAllProductToStocks(fileToRead);
                }
            } else {
                log.info("input folder is empty");
            }
        }else{
            log.info("folder={} doesnt exist", inputFolderPath);
            log.info("creating the folder={}", inputFolderPath);
            dir.mkdir();
        }
    }

    private void getAllProductToStocks(File file) throws ParserConfigurationException,
            IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile("boolean(/stocks/stock)");
        Boolean hasStock = (Boolean) expr.evaluate(document, XPathConstants.BOOLEAN);
        if(hasStock){
            List<StockDto> stocks = new ArrayList<>();
            XPathExpression exprResult = xpath.compile("/stocks/stock");
            NodeList nl = (NodeList) exprResult.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < nl.getLength(); i++){
                StockDto stockDto = new StockDto();
                Node childNode = nl.item(i);
                NodeList list = childNode.getChildNodes();
                for (int x = 0; x < list.getLength(); x++){
                    Node val = list.item(x);
                    if(val != null && !val.getNodeName().equalsIgnoreCase("#text")) {
                        if(val.getNodeName().contains("id")){
                            stockDto.setProductId(val.getTextContent());
                        }else if(val.getNodeName().contains("quantity")){
                            stockDto.setQuantity(val.getTextContent());
                        }
                    }
                }
                stocks.add(stockDto);
            }
            restockProduct(stocks);
        }
        moveTheFile(file);
        log.info("Product re-stocks completed");
    }

    private void restockProduct(List<StockDto> stocks){
        for(StockDto stock : stocks){
            Product product = productService.getProduct(Long.parseLong(stock.getProductId()));
            if(null != product) {
                product.setStocks(product.getStocks() + Integer.parseInt(stock.getQuantity()));
                productService.save(product);
            }
        }
        log.info("restocking completed");
    }

    public Optional<String> getExtension(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    private void moveTheFile(File file){
        File processFolder = new File(processFolderPath);
        if(!processFolder.exists()){
            processFolder.mkdir();
        }
        String filename = "stocks-" + DateTimeUtils.getTimeStamp()+ ".xml";
        if(file.renameTo(new File(processFolderPath+filename))){
            file.delete();
            log.info("File moved successfully");
        }
        else{
            log.info("Failed to move the file");
        }
    }
}
