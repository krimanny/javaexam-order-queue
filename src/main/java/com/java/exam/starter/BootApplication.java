package com.java.exam.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan("com.java.exam")
@EnableJpaRepositories("com.java.exam.dao")
@EntityScan("com.java.exam.entity")
@EnableScheduling
public class BootApplication  {

    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }
}
