package com.java.exam.service;

import com.java.exam.entity.OrderItem;

import java.util.List;

public interface OrderItemService {

    OrderItem save(OrderItem orderItem);

    List<OrderItem> findAllByOrderId(long id);

    int countAllStocksReserved(long id);
}
