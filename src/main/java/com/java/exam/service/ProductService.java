package com.java.exam.service;

import com.java.exam.entity.Product;

import java.util.List;

public interface ProductService {

    Product getProduct(long id);

    int insertProduct(Product product);

    List<Product> getAllProducts();

    int save(Product product);
}
