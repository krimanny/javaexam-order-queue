package com.java.exam.service;

import com.java.exam.entity.Order;

import java.util.List;

public interface OrderService {

    Order getOrder(long id);

    int insertOrders(Order order);

    List<Order> getAllOrders();

    Order save(Order order);

}
