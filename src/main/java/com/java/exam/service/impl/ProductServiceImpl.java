package com.java.exam.service.impl;

import com.java.exam.dao.ProductRepository;
import com.java.exam.entity.Product;
import com.java.exam.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product getProduct(long id) {
       Optional<Product> product = productRepository.findById(id);
       return product.isEmpty() ? null : product.get();
    }

    @Override
    public int insertProduct(Product product) {
        product =  productRepository.save(product);
        if(product != null){
            return 0;
        }
        return -1;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public int save(Product product) {
        productRepository.save(product);
        return 0;
    }

}
