package com.java.exam.service.impl;

import com.java.exam.dao.OrderItemRepository;
import com.java.exam.entity.OrderItem;
import com.java.exam.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemServiceImpl implements OrderItemService {

    @Autowired
    OrderItemRepository itemRepository;

    @Override
    public OrderItem save(OrderItem orderItem) {
        return itemRepository.save(orderItem);
    }

    @Override
    public List<OrderItem> findAllByOrderId(long orderId) {
        return itemRepository.findAllByOrderId(orderId);
    }

    @Override
    public int countAllStocksReserved(long id) {
         return  itemRepository.totalReserved(id);
    }
}
