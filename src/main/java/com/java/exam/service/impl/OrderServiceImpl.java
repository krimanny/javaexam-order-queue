package com.java.exam.service.impl;

import com.java.exam.dao.OrderRepository;
import com.java.exam.entity.Order;
import com.java.exam.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    public Order getOrder(long id) {
        Optional<Order> order = orderRepository.findById(id);
        return order.get();
    }

    @Override
    public int insertOrders(Order order) {
        order =  orderRepository.save(order);
        if(order != null){
            return 0;
        }
        return -1;
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }
}
