package com.java.exam.dao;

import com.java.exam.entity.OrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends PagingAndSortingRepository<OrderItem, Long> {

    List<OrderItem> findAllByOrderId(long orderId);

    @Query(value = "Select IFNULL((SELECT tp.stocks from t_product tp where id =:productId), 0)" +
            " - IFNULL(SUM(ti.quantity),0) as stocks from t_order_item ti " +
            "join t_order t1 on t1.id = ti.order_id where t1.status = 'RESERVED' ",
    nativeQuery = true)
    Integer totalReserved(@Param("productId") long productId);
}