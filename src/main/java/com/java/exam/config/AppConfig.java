package com.java.exam.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class AppConfig extends WebMvcConfigurationSupport {

    public static final String ORDER_QUEUE = "orders_queue";
    public static final String ORDER_RESPONSE_QUEUE = "orders_response_queue";
    public static final String ORDER_EXCHANGE = "orders_exchange";
    public static final String ROUTE_KEY = "orders_routingKey";
    public static final String RESPONSE_ROUTE_KEY = "response_orders_routingKey";

    @Bean
    public Queue queue(){
        return new Queue(ORDER_QUEUE);
    }

    @Bean
    public Queue queue2(){
        return new Queue(ORDER_RESPONSE_QUEUE);
    }

    @Bean
    public TopicExchange exchange(){
        return new TopicExchange(ORDER_EXCHANGE);
    }

    @Bean
    public Binding Binding(Queue queue, TopicExchange exchange){
        return BindingBuilder.bind(queue()).to(exchange).with(ROUTE_KEY);
    }

    @Bean
    public Binding Binding2(Queue queue, TopicExchange exchange){
        return BindingBuilder.bind(queue2()).to(exchange).with(RESPONSE_ROUTE_KEY);
    }

    @Bean
    public MessageConverter converter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory){
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}
