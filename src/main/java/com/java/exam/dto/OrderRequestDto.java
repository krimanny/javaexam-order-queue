package com.java.exam.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class OrderRequestDto implements Serializable {

    private String client_name;
    private List<OrderItemDto> items;

    @Override
    public String toString() {
        return "OrderRequest{" +
                "client_name='" + client_name + '\'' +
                ", items=" + items +
                '}';
    }
}
