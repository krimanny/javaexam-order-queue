package com.java.exam.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StockDto {

    private String productId;
    private String quantity;

    @Override
    public String toString() {
        return "StockDto{" +
                "productId='" + productId + '\'' +
                ", quantity='" + quantity + '\'' +
                '}';
    }
}
