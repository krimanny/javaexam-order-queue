package com.java.exam.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderItemDto {

    private long product_id;
    private int quantity;


    @Override
    public String toString() {
        return "OrderItem{" +
                "productId=" + product_id +
                ", quantity=" + quantity +
                '}';
    }
}
