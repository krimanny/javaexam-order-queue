package com.java.exam.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderResponseDto implements Serializable {

    private String order_id;
    private String order_status;
    private String error_message;

    public OrderResponseDto(){}

    public OrderResponseDto(String order_id, String order_status){
        this.order_id = order_id;
        this.order_status = order_status;
    }

    @Override
    public String toString() {
        return "OrderResponseDto{" +
                "order_id='" + order_id + '\'' +
                ", order_status='" + order_status + '\'' +
                ", error_message='" + error_message + '\'' +
                '}';
    }
}
