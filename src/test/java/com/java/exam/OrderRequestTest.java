package com.java.exam;

import com.java.exam.controller.OrderController;
import com.java.exam.dto.OrderItemDto;
import com.java.exam.dto.OrderRequestDto;
import com.java.exam.entity.Product;
import com.java.exam.service.OrderItemService;
import com.java.exam.service.OrderService;
import com.java.exam.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OrderRequestTest {

    @InjectMocks
    private OrderController orderController;

    @Mock
    private ProductService productService;

    @Mock
    private OrderService orderService;

    @Mock
    private OrderItemService orderItemService;

    @Mock
    RabbitTemplate rabbitTemplate;

    @Test
    public void requestOrder(){
        OrderRequestDto requestDto = new OrderRequestDto();
        List<OrderItemDto> items = new ArrayList<>();
        OrderItemDto item = new OrderItemDto();
        item.setProduct_id(1);
        item.setQuantity(10);
        items.add(item);
        requestDto.setClient_name("manny");
        requestDto.setItems(items);
        orderController.postOrder(requestDto);
    }

    @Test
    public void requestOrder_with_product(){
        OrderRequestDto requestDto = new OrderRequestDto();
        List<OrderItemDto> items = new ArrayList<>();
        OrderItemDto item = new OrderItemDto();
        item.setProduct_id(1);
        item.setQuantity(10);
        items.add(item);
        requestDto.setClient_name("manny");
        requestDto.setItems(items);

        Product product = new Product();
        product.setStocks(100);
        product.setName("product1");
        product.setId(1);

        Mockito.when(productService.getProduct(1)).thenReturn(product);
        orderController.postOrder(requestDto);
    }
}
